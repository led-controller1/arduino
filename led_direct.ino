#include <FastLED.h>
#define DATA_PIN 6
#define NUM_LEDS 8

CRGB leds[NUM_LEDS];

const byte numChars = 32;
char receivedChars[numChars];
char tempChars[numChars];    

byte command = 0;
int arg1 = 0;
int arg2 = 0;
int arg3 = 0;
int arg4 = 0;

boolean newData = false;

void setup() {
  //link.begin(9600); //setup software serial
  Serial.begin(9600);    //setup serial monitor
  FastLED.addLeds<WS2811, DATA_PIN, BRG>(leds, NUM_LEDS);

  leds[0] = CRGB::Blue; 
  leds[2] = CRGB::Red;
  FastLED.show();
}

void loop() {

  recvWithStartEndMarkers();
    if (newData == true) {
        strcpy(tempChars, receivedChars);
            // this temporary copy is necessary to protect the original data
            //   because strtok() used in parseData() replaces the commas with \0 LEDS_PER_TILE
        parseData();
        
        if (arg1 == -1 ) {
          FastLED.show(); 
        } else {
          leds[arg1].setRGB( arg2, arg3, arg4); 
        }
        
        newData = false;
    }




}


//============

void recvWithStartEndMarkers() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;

    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();

        if (recvInProgress == true) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        }

        else if (rc == startMarker) {
            recvInProgress = true;
        }
    }
}

//============

void parseData() {      // split the data into its parts

    char * strtokIndx; // this is used by strtok() as an index

    strtokIndx = strtok(tempChars,",");      // get the first part - the string
    arg1 = atoi(strtokIndx); 

    strtokIndx = strtok(NULL, ",");
    arg2 = atoi(strtokIndx);  
 
    strtokIndx = strtok(NULL, ","); // this continues where the previous call left off
    arg3 = atoi(strtokIndx);  

    strtokIndx = strtok(NULL, ",");
    arg4 = atoi(strtokIndx); 

}


